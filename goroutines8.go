package main

import (
	"fmt"
	"runtime"
)

func main() {
	runtime.GOMAXPROCS(1)
	// runtime.GOMAXPROCS(100)
	fmt.Printf("Number of thread avaibale: %v", runtime.GOMAXPROCS(-1))
	// By default is is equal to number of core in he machine

	/*
	- Do not create goroutines in libraries
		- let consumer control concurrency
	- When creating a goroutines, know how it will end
		- avoids subtle memory leaks
	- Check for race condition at compile time
		- go run -race goroutines2.go
	 */
}
