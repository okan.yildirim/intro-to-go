package main

import "fmt"

func main() {

	var w Writer = ConsoleWriter{} // polymorph
	w.Write([]byte("Hello Go!"))

	myInt := IntCounter(0)

	for i := 0; i < 10; i++ {
		fmt.Println(myInt.Increment())
	}
	// Type Conversion
/*	var wc WriterCloser = NewBufferedWriterCloser()
	bwc:= wc.(*BuffuredWriterCloser)*/


	var i interface{}=0
	switch i.(type) {

	case int :
		fmt.Println("i is an integer")
	case string:
		fmt.Println("i is an string")
	default:
		fmt.Println("we do not know")
	}
}

// interfaces describe behavior
type Writer interface {
	Write([]byte) (int, error)
}

type Closer interface {
	Close() error
}

type WriterCloser interface {
	Writer
	Closer
}

type ConsoleWriter struct{}

func (cw ConsoleWriter) Write(data []byte) (int, error) {
	n, err := fmt.Println(string(data))
	return n, err
}


type Incrementer interface {
	Increment() int
}

type IntCounter int

func (ic *IntCounter)Increment() int {
	*ic ++
	return int(*ic)
}