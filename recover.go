package main

import (
	"fmt"
	"log"
)

func main() {

	fmt.Println("start")

	defer func() {
		if err := recover(); err != nil {
			log.Println("Error", err)
		}
	}()

	panic("somethingbad happening")

	fmt.Println("end")

	// func5()

}

func func5() {

	fmt.Println("start")

	defer fmt.Println("defer")

	panic("somethingbad happening")

	fmt.Println("end")
}
