package main

import "fmt"

func main() {
	arr := []int{7, 6, 1, 5, 4, 2, 3, 9, 8}
	sort := insertionSort(arr)
	fmt.Print(sort)
}

func insertionSort(arr []int) []int {
	for i := 0; i < len(arr)-1; i++ {
		for j := i + 1; j < len(arr); j++ {
			if arr[i] > arr[j] {
				temp := arr[i]
				arr[i] = arr[j]
				arr[j] = temp
			}
		}
	}
	return arr
}
