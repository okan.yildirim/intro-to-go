package main

import "fmt"

type Stack []int

func (stack *Stack) isEmpty() bool {
	return len(*stack) < 1
}

func (stack *Stack) push(i int) {
	*stack = append(*stack, i)
}

func (stack *Stack) pop() (int, bool) {
	if stack.isEmpty() {
		return 0, false
	}
	index := len(*stack) -1
	element := (*stack)[index]
	*stack = append((*stack)[:index])
	return element, true
}
func main() {

	var stack Stack

	fmt.Printf("empty %v\n", stack.isEmpty())
	stack.push(3)
	stack.push(5)
	stack.push(8)

	fmt.Printf("empty after push %v\n", stack.isEmpty())

	fmt.Println(stack.pop())
	fmt.Println(stack.pop())
	fmt.Println(stack.pop())
	fmt.Printf("empty after pops %v\n", stack.isEmpty())

}
