package main

import "fmt"

type Queue []int

func (q *Queue) isEmpty() bool {
	return len(*q) < 1
}

func (q *Queue) enqueue(i int) {
	*q = append(*q, i)
}

func (q *Queue) dequeue() (int, bool) {
	if q.isEmpty() {
		return 0, false
	}
	head := (*q)[0]
	*q = append((*q)[1:])
	return head, true
}

func (q *Queue) display() {
	if q.isEmpty() {
		fmt.Println("Queue is empty!")
		return
	}
	for _, i := range *q {
		fmt.Printf("%v\t", i)
	}
	fmt.Println()
}

func main() {
	var queue Queue
	queue.display()

	queue.enqueue(3)
	queue.enqueue(5)
	queue.enqueue(7)

	queue.display()

	queue.dequeue()
	queue.display()

	queue.dequeue()
	queue.display()

	queue.dequeue()
	queue.display()
}
