package main

import (
	"fmt"
	"strconv"
)

type Node struct {
	val  int
	next *Node
}

func NewNode(val int) *Node {
	return &Node{
		val:  val,
		next: nil,
	}
}

type LinkedList struct {
	first *Node
	size  int
}

func NewLinkedList() LinkedList {
	return LinkedList{}
}

func (l *LinkedList) isEmpty() bool {
	return l.size == 0
}

func (l *LinkedList) addFirst(val int) {
	newNode := NewNode(val)
	if l.first == nil {
		l.first = newNode
	} else {
		newNode.next = l.first
		l.first = newNode
	}
	l.size++
}

func (l *LinkedList) addLast(val int) {
	newNode := NewNode(val)
	if l.first == nil {
		l.first = newNode
	} else {
		currentNode := l.first
		for currentNode.next != nil {
			currentNode = currentNode.next
		}
		currentNode.next = newNode
	}
	l.size++
}

func (l *LinkedList) removeFirst() {
	if !l.isEmpty() {
		l.first = l.first.next
		l.size--
	}
}

func (l *LinkedList) removeLast() {
	if !l.isEmpty() {
		if l.first.next == nil {
			l.first = nil
		} else {
			currentNode := l.first
			for currentNode.next.next != nil {
				currentNode = currentNode.next
			}
			currentNode.next = nil
		}
		l.size--
	}
}

func (l *LinkedList) toString() string {
	currentNode := l.first
	if currentNode == nil {
		return "[]"
	}
	s := "["
	for ; currentNode != nil; currentNode = currentNode.next {
		s = s + strconv.Itoa(currentNode.val) + ", "
	}
	s = s[:len(s)-2]
	s = s + "]"
	return s
}
func main() {
	linkedList := NewLinkedList()
	fmt.Println(linkedList.toString())
	linkedList.addFirst(5)
	fmt.Println(linkedList.toString())
	linkedList.addFirst(4)
	linkedList.addLast(6)
	fmt.Println(linkedList.toString())
	fmt.Println(linkedList.size)

	linkedList.removeFirst()
	linkedList.removeLast()
	fmt.Println(linkedList.toString())
	fmt.Println(linkedList.size)
}
