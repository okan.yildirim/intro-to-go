package main

import "fmt"

const a int16 = 23

const (
	k = iota
	l = iota
	m = iota
	n
)

const (
	t = iota
)

const (
	_ = iota + 5
	dog
	cat
)

const (
	_  = iota // ignore first value by assigning to blank identifier
	KB = 1 << (10 * iota)
	MB
	GB
	TB
	PB
	EB
	ZB
	YV
)

func main() {

	// fmt.Printf("My exportedVariable: %v and it's type: %T\n", myexportedvariable, myexportedvariable)

	const a int = 3
	fmt.Printf("My constat a: %v and it's type: %T\n", a, a)

	var b int = 7

	fmt.Printf("My constant sum: %v and it's typ: %T\n", a+b, a+b)

	fmt.Printf("My constat k: %v and it's type: %T\n", k, k)
	fmt.Printf("My constat l: %v and it's type: %T\n", l, l)
	fmt.Printf("My constat m: %v and it's type: %T\n", m, n)
	fmt.Printf("My constat n: %v and it's type: %T\n", n, n)

	fmt.Printf("My constat t: %v and it's type: %T\n", t, t)

	fmt.Printf("My constat dog: %v and it's type: %T\n", dog, dog)
	fmt.Printf("My constat cat: %v and it's type: %T\n", cat, cat)

	fileSize := 4000000000.
	fmt.Printf("%.2fGB", fileSize/GB)

}
