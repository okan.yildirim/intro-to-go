package main

import (
	"fmt"
	"net/http"
)

func main() {
	// function1()
	function2()
	function3()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello Go!"))
	})

	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		fmt.Println("My error")
		panic(err.Error())
	}
}

func function1() {
	a, b := 1, 0
	c := a / b
	fmt.Println(c)
}

func function2() {

	fmt.Println("start")
	panic("Something bad happening")
	fmt.Println("Finish")
}

func function3() {

	fmt.Println("start")
	defer fmt.Println("this was deffered")
	panic("Something bad happening")
	fmt.Println("Finish")
}
