package main

import (
	"fmt"
)

func main() {

	number := 70
	guess := 50
	min := 1
	max := 100

	if guess < min && isLimitControlEnable() && guess > max {
		fmt.Println("Guess should be between 1 and 100")
	} else {
		if guess < number {
			fmt.Println("It is too low")
		} else if guess > number {
			fmt.Println("It is too high")
		} else if guess == number {
			fmt.Println("yYs it is correct")
		}
	}

	myMap := map[string]int{
		"Ankara":   2343333,
		"İstanbul": 24534534}

	if pop, ok := myMap["İzmir"]; ok {
		fmt.Println(pop)
	}

	if pop, ok := myMap["Ankara"]; ok {
		fmt.Println(pop)
	}
}

func isLimitControlEnable() bool {
	fmt.Println("Limit control active")
	return true
}
