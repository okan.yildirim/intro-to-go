package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	func1()
	fmt.Println("****************************")
	func2()
	fmt.Println("****************************")
	func3()
	fmt.Println("****************************")

	res, err := http.Get("http://www.google.com/robots.txt")

	if err != nil{
		log.Fatal(err)
	}

	defer res.Body.Close() // most use case, do not forget to close, it is good to ve closer open resource
	robots, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s", robots)

}

func func1() {
	fmt.Println("start1")
	defer fmt.Println("middle1")
	fmt.Println("end1")
}


func func2() {
	defer fmt.Println("start2")
	defer fmt.Println("middle2")
	defer fmt.Println("end2")
}

func func3() {
	a:= "first"
	defer fmt.Println(a)
	a = "second"
}
