package main

import "fmt"

func main() {

	statePopulations := map[string]int {
		"Ankara":8250017,
		"Istanbul":20230017,
		"Izmir":5250017,
		"Antalya":2250017,
		"Sivas":950017,
	}

	fmt.Println(statePopulations)
	fmt.Println(len(statePopulations))
	fmt.Println(statePopulations["Ankara"])

	statePopulations["Edirne"] = 2344433
	// order is not guaranteed
	fmt.Println(statePopulations)

	delete(statePopulations, "Antalya")
	fmt.Println(statePopulations)
	fmt.Println(statePopulations["Antalya"])

	_, ok := statePopulations["wrfgfd"]
	fmt.Println(ok)
	m:= map[[3]int]int{}

	// arrays are OK, but slices are not
	fmt.Println(m)

	m2 := make(map[string]int)
	fmt.Println(m2)
	m3 := make(map[string]int,10)
	fmt.Println(m3)

	// pass by reference

	sp:= statePopulations
	delete(sp,"Sivas")
	fmt.Println(sp)
	fmt.Println(statePopulations)
}
