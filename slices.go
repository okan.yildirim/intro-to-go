package main

import "fmt"

func main() {
	numbers := []int{3, 5, 7}
	fmt.Printf("My slice: %v\n", numbers)
	fmt.Printf("My slice length: %v\n", len(numbers))
	fmt.Printf("My slice capacity: %v\n", cap(numbers))

	// Copies refer to same underlying data

	a := []int{3, 5, 7}
	b := a
	b[2] = 99
	fmt.Printf("a: %v\n", a)
	fmt.Printf("b: %v\n", b)


	x:= []int {1,2,3,4,5,6,7,8,9,10}

	y := x[:] // slices of all elements
	z := x[3:]
	t := x[:6]
	u := x[3:6]
	fmt.Printf("x: %v\n", x)
	fmt.Printf("y: %v\n", y)
	fmt.Printf("z: %v\n", z)
	fmt.Printf("t: %v\n", t)
	fmt.Printf("u: %v\n", u)


	points := make([]int, 3)

	fmt.Printf("My points: %v\n", points)
	fmt.Printf("My points length: %v\n", len(points))
	fmt.Printf("My points capacity: %v\n", cap(points))

	points2 := make([]int, 3, 100)

	fmt.Printf("My points2: %v\n", points2)
	fmt.Printf("My points2 length: %v\n", len(points2))
	fmt.Printf("My points2 capacity: %v\n", cap(points2))

	k := []int{}
	fmt.Printf("My k: %v\n", k)
	fmt.Printf("My k length: %v\n", len(k))
	fmt.Printf("My k capacity: %v\n", cap(k))

	k = append(k, 1)
	fmt.Printf("My k: %v\n", k)
	fmt.Printf("My k length: %v\n", len(k))
	fmt.Printf("My k capacity: %v\n", cap(k))

	k = append(k, 2,3,4,6)
	fmt.Printf("My k: %v\n", k)
	fmt.Printf("My k length: %v\n", len(k))
	fmt.Printf("My k capacity: %v\n", cap(k))

	k = append(k, []int{99,98,96}...)
	fmt.Printf("My k: %v\n", k)
	fmt.Printf("My k length: %v\n", len(k))
	fmt.Printf("My k capacity: %v\n", cap(k))

	k = append(k, x...)
	fmt.Printf("My k: %v\n", k)
	fmt.Printf("My k length: %v\n", len(k))
	fmt.Printf("My k capacity: %v\n", cap(k))


	l:= []int {1,2,3,4,5}
	// remove start
	m := l[1:]
	fmt.Printf("m: %v\n", m)

	// remove end
	n := l[:len(l)-1]
	fmt.Printf("n: %v\n", n)

	// remove middle
	o := append(l[:2], l[3:]...)
	fmt.Printf("o: %v\n", o)
	// Bevare of  reference
	fmt.Printf("l: %v\n", l)










}

