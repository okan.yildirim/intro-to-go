package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("Number of thread avaibale: %v", runtime.GOMAXPROCS(-1))
	// By default is is equal to number of core in he machine
}
