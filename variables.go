package main

import "fmt"
import "strconv"

var globalVariable int = 5
var myexportedvariable string = "okan.yildirim"

func main() {
	fmt.Println("Hello World!")

	var myNumber int = 3
	fmt.Printf("myNumber: %v\n", myNumber)

	floatNumber := 32.5
	fmt.Printf("My float Number: %v and it's type: %T\n", floatNumber, floatNumber)

	var float32Number float32 = float32(myNumber)
	fmt.Printf("My float32 Number: %v and it's type: %T\n", float32Number, float32Number)

	var myStringNumber string
	myStringNumber = strconv.Itoa(myNumber);
	fmt.Printf("My string Number: %v and it's type: %T\n", myStringNumber, myStringNumber)

	fmt.Printf("My globalVariable: %v and it's type: %T\n", globalVariable, globalVariable)

	/*

	* Can't redeclare variables, but can shadow them
	* All variables must be used

	* Visibility
		* lower case first letter for package scope
		* upper case first letter to export
		* no private scope

	*/

}
