package main

import (
	"fmt"
	"sync"
)

var wg = sync.WaitGroup{}
func main() {
	a:= "Okan"
	wg.Add(1)

	go func (a string) {
	fmt.Println(a)
	wg.Done()
	}(a)

	a="Oki"
	wg.Wait()
}

