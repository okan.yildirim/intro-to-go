package main

import "fmt"

func main() {

	switch 2 {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("two")
	default:
		fmt.Println("not one or two")
	}

	switch 3 {
	case 1,3,5,7,9:
		fmt.Println("odd")
	case 2,4,6,8:
		fmt.Println("even")
	default:
		fmt.Println("not one or two")
	}

	switch i := 2+ 4; i {
	case 1,3,5,7,9:
		fmt.Println("odd")
	case 2,4,6,8:
		fmt.Println("even")
	default:
		fmt.Println("not one or two")
	}

	j:=10

	switch  {
	case j <= 10:
		fmt.Println("less than or equal to ten")
		// fallthrough
	case j <= 20:
		fmt.Println("twenty")
	}

	var k interface{} = 1
	switch k.(type){
	case int:
		fmt.Println("int")
	case float32:
		fmt.Println("float32")
	case string:
		fmt.Println("string")
	default:
		fmt.Println("another type")
	}
}
