package main

import (
	"fmt"
	"sync"
)

var counter4 = 0
var wg4 = sync.WaitGroup{}

func main() {
	for i := 0; i < 10; i++ {
		wg4.Add(2)
		go sayHi4()
		go increment4()
	}
	wg4.Wait()
}

func sayHi4() {
	fmt.Printf("Hi #%v\n", counter4)
	wg4.Done()
}

func increment4() {
	counter4++
	wg4.Done()
}
