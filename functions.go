package main

import (
	"fmt"
)

func main() {

	sayMessage("Hello Go!")
	name := "okan"
	sayHelloTo("Hello, ", name)
	sayHelloTo2("Hello, ", name)
	fmt.Println(name)

	hi := "hi"
	sayHelloTo3(&hi, &name)
	fmt.Println(name)

	fmt.Println("************************************")

	sum := sum(1, 2, 3, 4, 5)
	fmt.Println(sum)

	sum2 := sum2(1, 2, 3, 4, 5)
	fmt.Println(*sum2)

	sum3 := sum3(1, 2, 3, 4, 5)
	fmt.Println(sum3)


	d, err := divide(5.0, 3.0)

	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(d)

	func(){
		fmt.Println("Anonymous function")
	}()


	for i:=0; i< 5; i++ {
		func(){
			fmt.Println(i)
		}()
	}

	for i:=0; i< 5; i++ {
		func(i int){
			fmt.Println(i)
		}(i)
	}

	f := func() {
		fmt.Println("Anonymous function 2")
	}
	f()


	var div func( float64, float64) (float64, error)
	div = func(a float64, b float64) (float64, error) {
		if b == 0 {
			return 0.0, fmt.Errorf("Cannot divide by zero")
		}
		return a/b, nil
	}

	di, err := div(5.0, 3.0)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(di)

	fmt.Println("**************Methods******************")

	g:= greeter{
		greeting: "hello",
		name:     "okidoki",
	}

	g.greet()
	fmt.Println(g.name)
	g.greet2()
	fmt.Println(g.name)

}

func sayMessage(msg string) {
	fmt.Println(msg)
}

func sayHelloTo(greeting, name string) {
	fmt.Println(greeting, name)
}

func sayHelloTo2(greeting, name string) {
	fmt.Println(greeting, name)
	name = "oki"
	fmt.Println(name)
}

func sayHelloTo3(greeting *string, name *string) {
	fmt.Println(*greeting, *name)
	*name = "oki"
	fmt.Println(*name)
}

func sum(values ...int) int  {
	result := 0
	for _, v := range values {
		result += v
	}
	fmt.Printf("Result is %v\n" , result)
	return result
}

func sum2(values ...int) *int  {
	result := 0
	for _, v := range values {
		result += v
	}
	fmt.Printf("Result is %v\n" , result)
	return &result
}

func sum3(values ...int) (result int)  {
	for _, v := range values {
		result += v
	}
	fmt.Printf("Result is %v\n" , result)
	return
}

func divide(a float64, b float64) (float64, error) {
	if b == 0 {
		return 0.0, fmt.Errorf("Can not divide by zero")
	}
	return a/b, nil
}


type greeter struct {
	greeting string
	name string
}

func (g greeter) greet()   {
	fmt.Println(g.greeting, g.name)
	g.name = "okiiii"
}

func (g *greeter) greet2()   {
	fmt.Println(g.greeting, g.name)
	g.name = "okiiii"
}