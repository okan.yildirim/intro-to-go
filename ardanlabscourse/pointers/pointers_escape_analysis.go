package main

type user struct {
	name  string
	email string
}

func main() {

	u1 := createUserV1()
	u2 := createUserV2()

	println("u1", &u1, "u2", u2)
}

// creates a user value and passed a copy back to the caller
func createUserV1() user {

	u1 := user{
		name:  "okan",
		email: "okan@email.com",
	}

	println("V1", &u1)
	return u1
}

// creates a user value and shares the value to the caller
func createUserV2() *user {

	u1 := user{
		name:  "okan",
		email: "okan@email.com",
	}

	println("V2", &u1)
	return &u1
}

// Static Code Analysis, Escape Analysis is not about where value construction but how value is shared
// If value is shared up (function return a value), it figures out construction will be in heap, because stack will be deleted.
// Then value will be deleted from heap by garbage collector
