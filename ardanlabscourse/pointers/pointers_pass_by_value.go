package main

func main() {

	count := 10
	println("count: \t Value: Of [", count, "]\t Addr Of [", &count, "]")

	// Pass the "value of" the count
	increment(count)
	println("count: \t Value: Of [", count, "]\t Addr Of [", &count, "]")

}

func increment(count int) {
	count++
	println("count: \t Value: Of [", count, "]\t Addr Of [", &count, "]")
}

// Value semantic is good but nothing is free
// Cost of it is efficiency, multiple threads reach and write on value, provide consistent data -> it is complex (copy)

/*
	 Stack
     -----
Main | 10 | 0x2
     -----
Incr | 11 | 0x1
	 -----
*/
