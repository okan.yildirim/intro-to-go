package main

func main() {

	count := 10
	println("count: \t Value: Of [", count, "]\t Addr Of [", &count, "]")

	// Pass the "address of" the count
	increment2(&count)
	println("count: \t Value: Of [", count, "]\t Addr Of [", &count, "]")

}

func increment2(count *int) {
	*count++
	println("count: \t Value: Of [", count, "]\t Addr Of [", &count, "] \t Value Points Of to [", *count, "]")
}

// Reaching from own frame to another is the side effect but is efficiency
/*
	 Stack
     -----
Main | 10 | 10 + 1    0x2
     -----	  ^
Incr | *  |-->|   incr reach out the address
	 -----
*/
