package main

// Number of elements to grow each stack frame
// run with 10 and then with 1024
const size = 10

func main() {

	s := "HELLO"

	stackCopy(&s, 0, [size]int{})

}

// stackCopy recursively runs increasing the size of the stack
func stackCopy(s *string, c int, a [size]int) {
	println(c, s, *s)
	c++
	if c == 10 {
		return
	}
	stackCopy(s, c, a)

}

// 2K --> 4K stack is enlarging, copy cost

/*
	4 Meg									6 Meg

	2 Meg GAP			After GC			3 Meg

	2 Meg in Use							3 Meg

*/

// GC NOTES

// Passor is a big important part of GC, is a job to figure out when collection start, takes how many times
// measure the pressure on heap -> how we quickly fill the heap up between GC  perform
// GC are also goroutines, we give dedicated %25 cpu to GC
// The less heap, the less the GC spend, the more speed but number of GC will increase. Optimize it.
