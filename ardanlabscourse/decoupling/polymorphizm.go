package main

import "fmt"

type reader interface {
	read(a []byte) string
}

type csvReader struct {
}

func (c csvReader) read(a []byte) string {
	return string(a) + ".csv"
}

type xmlReader struct {
}

func (x xmlReader) read(a []byte) string {
	return string(a) + ".xml"
}

func main() {

	csvReader := csvReader{}
	xmlReader := xmlReader{}

	name := "okan"
	bytes := []byte(name)

	fmt.Println(csvReader.read(bytes))
	fmt.Println((&csvReader).read(bytes))

	fmt.Println(xmlReader.read(bytes))
	fmt.Println((&xmlReader).read(bytes))

	fmt.Println(readBytes(csvReader, bytes))
	fmt.Println(readBytes(xmlReader, bytes))

	fmt.Println(readBytes(&csvReader, bytes))
	fmt.Println(readBytes(&xmlReader, bytes))

}

func readBytes(r reader, a []byte) string {
	return r.read(a)
}
