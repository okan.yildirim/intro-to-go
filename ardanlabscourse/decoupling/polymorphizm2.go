package main

import "fmt"

type printer interface {
	print()
}

type user struct {
	name string
}

func (u user) print() {
	fmt.Printf("UserName: %s\n", u.name)
}

func main() {

	u := user{name: "okan"}

	printers := []printer{u, &u}

	u.name = "okan.changed"
	for _, p := range printers {
		p.print()
	}

	// When we store a value, the interface value has its own copy of the value
	// Changes to the original value will not be seen
}
