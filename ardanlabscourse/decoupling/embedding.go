package main

import (
	"fmt"
)

type greeter interface {
	greeting()
}
type human struct {
	name string
}

func (h *human) greeting() {
	fmt.Println("Hello, my name is " + h.name)
}

type manager struct {
	h     human // this is NOT Embedded Type
	title string
}

// leader represents a human user with privileges
type leader struct {
	human // this is NOT Embedded Type
	title string
}

type admin struct {
	human // this is NOT Embedded Type
	title string
}

func (a admin) greeting() {
	fmt.Println("Hello, my name is " + a.name + ". I am an Admin")
}

type ceo struct {
	human
}

func main() {

	m := manager{
		h:     human{"Jane"},
		title: "Finance Manager",
	}

	m.h.greeting()

	l := leader{
		human: human{"okan"},
		title: "Technology Manager",
	}

	// the inner type method is promoted
	l.greeting()

	// We can access the inner type's method directly
	l.human.greeting()

	// leader is not a subtype of human like OOP

	sayHello(&l)

	a := admin{human{name: "bill"}, "admin"}

	a.human.greeting()
	a.greeting()
}

func sayHello(g greeter) {
	g.greeting()
}
