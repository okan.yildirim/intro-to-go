package main

import "fmt"

// it is okay to value semantics to reference semantics
// but is never okay from pointer semantics to value
// if the value is shared with you, don't copy it

type notifier interface {
	notify()
}

type email struct {
	message string
}

func (e *email) notify() {
	fmt.Println("Notifier message is " + e.message)
}

type slack struct {
	message string
}

func (s slack) notify() {
	fmt.Println("Notifier message is " + s.message)

}

func main() {

	email := email{message: "Email is sent."}
	slack := slack{message: "Slack message is sent."}

	email.notify()
	(&email).notify()

	slack.notify()
	(&slack).notify()
	/*
		./valuevsreference.go:38:8: cannot use email (type email) as type notifier in argument to notify:
			email does not implement notifier (notify method has pointer receiver)
	*/

	// notify(email)
	notify(slack)
}

func notify(n notifier) {
	n.notify()
}
