package main

const (
	// Max integer value on 64 bit architecture
	maxInt = 9223372036854775807

	// Much larger value than int64
	bigger = 9223372036854775808543522345

	//will NOT compile
	// biggerInt int64 = 9223372036854775808543522345
)

func main() {

	// Untyped constants, compile implicitly convert
	const ui = 12345    // kind: integer
	const uf = 3.141592 // king: floating-point

	// Types constants still use the constant type system but their p is restricted

	const ti int = 12345        // type: int
	const tf float64 = 3.141592 // type: float64

	// ./constants.go:XX constant 1000 overflows uint8
	// const myUint8 uint8 = 1000

	// Constants arithmetic supports different kinds.

	// Kind promotion is used to determine kind in these scenarios

	// Constants are not just read only variable their precision abilities are high

	// Variable answer will of type float64

	var answer = 3 * 0.333 // KindFloat(3) * KindFloat(0.333)

	// Constant third will be of kind floating point

	const third = 1 / 30 // KindFloat(1) / KindFloat(3.0)

	println(answer)

}
