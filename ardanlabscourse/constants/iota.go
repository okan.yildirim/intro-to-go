package main

import "fmt"

func main() {

	const (
		A1 = iota // 0
		B1 = iota // 1
		C1 = iota // 2
	)

	fmt.Println("1:", A1, B1, C1)

	const (
		A2 = iota // 0
		B2        // 1
		C2        // 2
	)

	fmt.Println("2:", A2, B2, C2)

	const (
		A3 = iota + 1 // 0
		B3            // 1
		C3            // 2
	)

	fmt.Println("3:", A3, B3, C3)

	const (
		Ldate         = 1 << iota // 1 Shift 1 to the left 0. 0000 0001
		Ltime                     // 1 Shift 1 to the left 1. 0000 0010
		Lmicrosecodes             // 1 Shift 1 to the left 2. 0000 0100
		Llongfile                 // 1 Shift 1 to the left 3. 0000 1000
		Lshortfile                // 1 Shift 1 to the left 4. 0001 0000
		LUTC                      // 1 Shift 1 to the left 5. 0010 0000
	)

	fmt.Println("Log: ", Ldate,
		Ltime,
		Lmicrosecodes,
		Llongfile,
		Lshortfile,
		LUTC)

}
