package main

import "fmt"

type example struct {
	flag bool
	// [3] byte padding in the memory allocation
	counter int16
	pi      float32
}

// always order your fields largest to smallest. first ->

func main() {
	// Data oriented approach

	var e1 example

	fmt.Printf("%+v\n", e1)

	e2 := example{
		flag:    true,
		counter: 10,
		pi:      3.141592,
	}

	fmt.Println("Flag", e2.flag)
	fmt.Println("Counter", e2.counter)
	fmt.Println("Pi", e2.pi)

	e3 := example{}

	fmt.Printf("%+v\n", e3) // the same as zero value

	// Declare a variable of an anonymous type set to its zero value

	var a1 struct {
		flag    bool
		counter int16
		pi      float32
	}

	fmt.Printf("%+v\n", a1)

	// Declare a variable of an anonymous type and init using a struct literal
	var a2 = struct {
		flag    bool
		counter int16
		pi      float32
	}{
		flag:    true,
		counter: 10,
		pi:      3.141592,
	}

	fmt.Println("Flag", a2.flag)
	fmt.Println("Counter", a2.counter)
	fmt.Println("Pi", a2.pi)

	/*

		type bill struct {
			flag bool
			counter int16
			pi      float32
		}

		type okan struct {
				flag bool
				counter int16
				pi      float32
			}

		var b bill
		var o okan

		b = o // error

		b = bill(o) // success
	*/
}
