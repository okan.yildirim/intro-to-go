package main

import "fmt"

func main() {

	// Type is life
	// Use type accurately, consistently and efficiently

	// Declare variables that are set to their zero value

	var a int
	var b string
	var c float64
	var d bool

	fmt.Printf("var a int \t %T [%v] \n", a, a)
	fmt.Printf("var b int \t %T [%v] \n", b, b)
	fmt.Printf("var c int \t %T [%v] \n", c, c)
	fmt.Printf("var d int \t %T [%v] \n", d, d)

	// Declare variables and initialize

	aa := 10
	bb := "hello"
	cc := 3.14159
	dd := true

	fmt.Printf("aa := 10 \t %T [%v] \n", aa, aa)
	fmt.Printf("bb := \"hello\" \t %T [%v] \n", bb, bb)
	fmt.Printf("cc := 3.14159 \t %T [%v] \n", cc, cc)
	fmt.Printf("dd := true \t %T [%v] \n", dd, dd)

	/*

		string zero value ->>>>    -----
		                           |nil|
		                           -----
		                           |   |
		                           -----

		string hello  [h,e,l,l,o] 		 			 -----
				       |________________________>>>> | * |
													 -----
													 | 5 |
													 -----
	*/

	// casting is risky, conversion is better

	// Specify tyoe and perform a conversion

	aaa := int32(10)
	fmt.Printf("aaa := int32(10) \t %T [%v] \n", aaa, aaa)

}
