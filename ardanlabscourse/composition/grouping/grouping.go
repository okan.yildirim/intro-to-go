package main

import "fmt"

type animal struct {
	name     string
	isMammal bool
}

func (a animal) speak() {
	fmt.Printf("UGH! My name is %s, it is %t I am a mammal\n", a.name, a.isMammal)
}

type dog struct {
	animal
	age int
}

func (d *dog) speak() {
	fmt.Printf("Woof! My name is %s, it is %t I am a mammal with age %d\n", d.name, d.isMammal, d.age)
}

type cat struct {
	animal
	climbFactor int
}

func main() {

	d := dog{
		animal: animal{"karabas", true},
		age:    7,
	}

	c := cat{
		animal:      animal{"karamel", true},
		climbFactor: 4,
	}

	// animals := []animal{d, c}

	// ./grouping.go:40:22: cannot use d (type dog) as type animal in slice literal
	//./grouping.go:40:25: cannot use c (type cat) as type animal in slice literal
	d.speak()
	c.speak()
}

// Smells:
// * The animal type is providing an abstraction layer of reusable state.
// * The program never needs to create or solely use a value type of Animal
// * The implementation of the Speak method for tge Animal type is a generalization
// * The Speak method for the Animal type is never going to be called

// Focus on the behavior, use interface

type Speaker interface {
	speak()
}

// Here are some guidelines around declaring types:
// * Declare types that represent sth new or unique
// * Validate that a value of any type is created ır used on its own
// * Embed types to reuse existing behaviours you need to satisfy
// * Question types that are an alias or abstraction for an existing type.
// * Question types  whose sole purpose is to share common state
