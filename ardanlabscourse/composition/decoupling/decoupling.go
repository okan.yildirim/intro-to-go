package main

import (
	"errors"
	"fmt"
	"io"
	"math/rand"
	"time"
)

type Data struct {
	Line string
}

type Puller interface {
	Pull(d *Data) error
}

type Storer interface {
	Store(d *Data) error
}

// PullStorer No need after the refactoring
type PullStorer interface {
	Puller
	Storer
}

// Xenia is a system we need to pull data from
type Xenia struct {
	Host    string
	Timeout time.Duration
}

func (x *Xenia) Pull(d *Data) error {
	r := rand.Intn(10)
	switch r {
	case 2:
		return io.EOF
	case 5:
		return errors.New("error reading data from Xenia")
	default:
		d.Line = "Data"
		fmt.Println("In: ", d.Line)
		return nil
	}
}

// Pillar is a system we need to store data into Pillar
type Pillar struct {
	Host    string
	Timeout time.Duration
}

func (p *Pillar) Store(d *Data) error {
	fmt.Println("Out:", d.Line)
	return nil
}

/*// System wraps Xenia and Pillar together into a single system      No need after the refactoring
type System struct {
	Puller
	Storer
}*/

// pull knows how to pull bulks of data from Xenia
func pull(p Puller, data []Data) (int, error) {

	for i := range data {
		if err := p.Pull(&data[i]); err != nil {
			return i, err
		}
	}
	return len(data), nil
}

// store knows how to store bulks of data into Pillar
func store(p Storer, data []Data) (int, error) {

	for i := range data {
		if err := p.Store(&data[i]); err != nil {
			return i, err
		}
	}
	return len(data), nil
}

// copy knows how to pull and store data from the System
func copy(p Puller, s Storer, batch int) error {

	data := make([]Data, batch)

	for {
		i, err := pull(p, data)
		if i > 0 {
			if _, err := store(s, data); err != nil {
				return err
			}
		}

		if err != nil {
			return err
		}
	}
}
func main() {

	x := Xenia{
		Host:    "localhost:8000",
		Timeout: time.Second,
	}
	p := Pillar{
		Host:    "localhost:9000",
		Timeout: time.Second,
	}

	if err := copy(&x, &p, 3); err != io.EOF {
		fmt.Println(err)
	}
}
