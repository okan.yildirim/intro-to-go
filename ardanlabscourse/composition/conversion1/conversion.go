package main

import "fmt"

type Mover interface {
	Move()
}

type Locker interface {
	Lock()
	Unlock()
}

type MoveLocker interface {
	Mover
	Locker
}

type bike struct {
}

func (bike) Move() {
	fmt.Println("Move")
}

func (bike) Lock() {
	fmt.Println("Lock")
}

func (bike) Unlock() {
	fmt.Println("Unlock")
}
func main() {

	var m Mover
	var ml MoveLocker

	ml = bike{}

	// An iterface value of type MoveLocker can be implicitly converted into
	//a value of type Mover. They both declare a method named move.
	m = ml

	// Cannot use 'm' (type Mover) as the type MoveLocker Type does not implement 'MoveLocker' as some methods are missing:

	// ./conversion.go:45:7: cannot use m (variable of type Mover) as type MoveLocker in assignment:
	//	Mover does not implement MoveLocker (missing Lock method

	//ml = m
	fmt.Println(m)
}
