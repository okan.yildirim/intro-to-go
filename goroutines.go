package main

import (
	"fmt"
	"time"
)

func main() {
	a:= "Okan"
	go sayHello()
	time.Sleep(1000 * time.Microsecond)

	go func (a string) {
		fmt.Println(a)
	}(a)

	a="Oki"
}

func sayHello() {
	fmt.Println("Hello")
}
