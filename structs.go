package main

import "fmt"
import "reflect"

type Doctor struct {
	number     int
	actorName  string
	companions []string
}

type Animal struct {
	Name string
	Origin string
}

type Bird struct {
	Animal
	SpeedKPH float32
	CanFly bool
}

type People struct {
	Name string  `required max:"100"`
	Origin string
}

func main() {

	aDoctor := Doctor{
		number:    3,
		actorName: "actorName",
		companions: []string{
			"Liz Shaw",
			"Jo Grant",
		},
	}

	fmt.Println(aDoctor)
	fmt.Println(aDoctor.actorName)

	aDoctor2 := Doctor{
		3,
		"actorName",
		[]string{
			"Liz Shaw",
			"Jo Grant",
		},
	}

	fmt.Println(aDoctor2)

	anActor := struct {
		name string
		age int
	}{name: "Tom Cruise", age: 45}

	fmt.Println(anActor)

	anotherActor := anActor
	anotherActor.name = "Okan"
	fmt.Println(anActor)
	fmt.Println(anotherActor)

	anotherActor2 := &anActor
	anotherActor2.name = "John"

	fmt.Println(anActor)
	fmt.Println(anotherActor2)

	fmt.Println("**************** Inheritance ********************")
	// no inheritance but use composition via embedding
	aBird := Bird{}
	aBird.Name = "penguen"
	aBird.Origin = "North Denmark"
	aBird.SpeedKPH = 0
	aBird.CanFly = false

	fmt.Println(aBird)

	fmt.Println("**************** Tag ********************")
	// Tags can be added to struct fields to describe fields

	t:= reflect.TypeOf(People{})
	field, _ := t.FieldByName("Name")
	fmt.Println(field.Tag)
}
