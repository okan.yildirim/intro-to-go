package main

import (
	"fmt"
)

func main()  {

	// Bool
	fmt.Println("*************** Bool ***************")

	var t bool
	fmt.Printf("My booleanVariable: %v and it's type: %T\n", t, t)

	var k bool = true
	fmt.Printf("My booleanVariable: %v and it's type: %T\n", k, k)

	n:= 1 == 1
	m:= 1 == 2

	fmt.Printf("My booleanVariable: %v and it's type: %T\n", n, n)
	fmt.Printf("My booleanVariable: %v and it's type: %T\n", m, m)

	// Numeric
	fmt.Println("*************** Numeric ***************")

	a:=10
	b:=3
	fmt.Println( a + b)
	fmt.Println( a - b)
	fmt.Println( a * b)
	fmt.Println( a / b)
	fmt.Println( a % b)

	fmt.Println("******************************")

	a =10 // 1010
	b =3 // 0011
	fmt.Println( a & b) // 0010
	fmt.Println( a | b) // 1011
	fmt.Println( a ^ b) // 1001
	fmt.Println( a &^ b) // 1100

	fmt.Println("******************************")

	var c int =10
	var d uint8= 32
	fmt.Println( c + int (d)) // types must match

	fmt.Println("******************************")

	e := 8 // 2^3
 	fmt.Println( e << 3) // 2^3 * 2^3 = 2^6
	fmt.Println( e >> 3) // 2^3 / 2^3 = 2^0

	fmt.Println("*************** Numeric ***************")

	var name string = "okan"
	fmt.Printf("My string: %v and it's type: %T\n", name, name)
	fmt.Printf("My string: %v and it's type: %T\n", string(name[1]), name[1])

	var surname string = "yildirim"

	fmt.Println("Concat: " + name + surname)

    byteArray := []byte(name)
	fmt.Printf("%v %T\n", byteArray, byteArray)


}