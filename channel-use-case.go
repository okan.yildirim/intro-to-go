package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const productEndpoint = "https://fakestoreapi.com/products/1"
const userEndpoint = "https://fakestoreapi.com/users/1"

type product struct {
	Name  string  `json:"title"`
	Price float32 `json:"price"`
}

type user struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}

type order struct {
	Username     string  `json:"username"`
	UserEmail    string  `json:"userEmail"`
	ProductName  string  `json:"productName"`
	ProductPrice float32 `json:"productPrice"`
}

var productChannel = make(chan product)
var userChannel = make(chan user)

func main() {

	go getProduct()
	go getUser()

	product := <-productChannel
	user := <-userChannel

	order := order{
		Username:     user.Username,
		UserEmail:    user.Email,
		ProductName:  product.Name,
		ProductPrice: product.Price,
	}

	fmt.Println(order)

}

func getProduct() {

	client := http.Client{}
	response, err := client.Get(productEndpoint)
	if err != nil {
		panic(err)
	}
	var product product
	err = json.NewDecoder(response.Body).Decode(&product)
	if err != nil {
		panic(err)
	}
	productChannel <- product

}

func getUser() {
	client := http.Client{}
	response, err := client.Get(userEndpoint)
	if err != nil {
		panic(err)
	}
	var user user
	err = json.NewDecoder(response.Body).Decode(&user)
	if err != nil {
		panic(err)
	}
	userChannel <- user
}
