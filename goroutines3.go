package main

import (
	"fmt"
	"sync"
)

var counter3 = 0
var wg3 = sync.WaitGroup{}

func main() {
	for i := 0; i < 10; i++ {
		wg3.Add(2)
		go sayHi()
		go increment()
	}
	wg3.Wait()
}

func sayHi() {
	fmt.Printf("Hi #%v\n", counter3)
	wg3.Done()
}

func increment() {
	counter3++
	wg3.Done()
}
