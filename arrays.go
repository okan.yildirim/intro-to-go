package main

import "fmt"

func main() {
	// Arrays are fixed size whereas slices not
	numbers := [3]int{3, 5, 7}
	fmt.Printf("My numbers: %v\n", numbers)

	numbers2 := [...]int{3, 5, 7, 9}
	fmt.Printf("My numbers2: %v\n", numbers2)
	numbers2[3] = 8
	fmt.Printf("My numbers2: %v\n", numbers2)

	var numbers3 [3]int
	fmt.Printf("My numbers3: %v\n", numbers3)
	fmt.Printf("My numbers3 length: %v\n", len(numbers3))

	var students [3]string
	fmt.Printf("My students: %v\n", students)
	fmt.Printf("My students length: %v\n", len(students))

	// Copies refer to different underlying data
	var identityMatrix = [3][3]int{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}
	fmt.Printf("My identityMatrix: %v\n", identityMatrix)

	var identityMatrix2 [3][3]int
	identityMatrix2[0] = [3]int{1, 0, 0}
	identityMatrix2[1] = [3]int{0, 1, 0}
	identityMatrix2[2] = [3]int{0, 0, 1}

	fmt.Printf("My identityMatrix2: %v\n", identityMatrix2)


	a := [3]int{3, 5, 7}
	b := a
	b[2] = 99
	fmt.Printf("a: %v\n", a)
	fmt.Printf("b: %v\n", b)


	c := [3]int{3, 5, 7}
	d := &c
	d[2] = 99
	fmt.Printf("c: %v\n", c)
	fmt.Printf("d: %v\n", d)

	x:= [...]int {1,2,3,4,5,6,7,8,9,10}

	y := x[:] // slices of all elements
	z := x[3:]
	t := x[:6]
	u := x[3:6]
	fmt.Printf("x: %v\n", x)
	fmt.Printf("y: %v\n", y)
	fmt.Printf("z: %v\n", z)
	fmt.Printf("t: %v\n", t)
	fmt.Printf("u: %v\n", u)
}
