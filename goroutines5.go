package main

import (
	"fmt"
	"sync"
)

var counter5 = 0
var wg5 = sync.WaitGroup{}
var mutex = sync.RWMutex{} // for synchronization by locking

func main() {
	for i := 0; i < 10; i++ {
		wg5.Add(2)
		go sayHi5()
		go increment5()
	}
	wg5.Wait()
}

func sayHi5() {
	mutex.RLock()
	fmt.Printf("Hi #%v\n", counter5)
	mutex.RUnlock()
	wg5.Done()
}

func increment5() {
	mutex.Lock()
	counter5++
	mutex.Unlock()
	wg5.Done()
}
