package main

import "fmt"

func main() {
	a := 42
	b := a
	fmt.Println(a, b)
	a = 5
	fmt.Println(a, b)




	var c int = 42
	var d *int = &c
	fmt.Println(c, d)
	fmt.Println(&c, d)
	fmt.Println(c, *d)

	c = 22
	fmt.Println(c, *d)

	*d = 11
	fmt.Println(c, *d)





	e := [3]int{1, 2, 3}
	f := &e[0]
	g := &e[1]
	fmt.Printf("%v %p %p\n", e, f, g)


	var ms1 myStruct
	ms1 =  myStruct{foo: 42}
	fmt.Println(ms1)

	var ms2 *myStruct
	ms2 =  &myStruct{foo: 42}
	fmt.Println(ms2)

	var ms3 *myStruct
	ms3 = new(myStruct)
	fmt.Println(ms3)

	ms3.foo = 44
	fmt.Println(ms3.foo)
}

type myStruct struct {
	foo int
}