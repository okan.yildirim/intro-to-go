package main

import (
	"fmt"
	"sync"
)

var counter6 = 0
var wg6 = sync.WaitGroup{}
var mutex6 = sync.RWMutex{} // for synchronization by locking

func main() {
	for i := 0; i < 10; i++ {
		wg6.Add(2)
		mutex6.RLock()
		go sayHi6()
		mutex6.Lock()
		go increment6()
	}
	wg6.Wait()
}

func sayHi6() {
	fmt.Printf("Hi #%v\n", counter6)
	mutex6.RUnlock()
	wg6.Done()
}

func increment6() {
	counter6++
	mutex6.Unlock()
	wg6.Done()
}
