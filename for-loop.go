package main

import (
	"fmt"
)

func main() {

	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}

	for i, j := 0, 0; i < 5; i, j = i+1, j+2 {
		fmt.Println(i, j)
	}

	k := 0
	for ; k < 3; k++ {
		fmt.Println(k)
	}

	for  {
		fmt.Println("Infinite loop",k)

		if k > 5 {
			break
		}
		k++
	}


	s:= [3]string {"okan", "yildirim", "go"}

	for index, value := range s{
		fmt.Println(index,value)
	}

	w:= "hello"
	for index, value := range w{
		fmt.Println(index,value)
	}


	statePopulations := map[string]int {
		"Ankara":8250017,
		"Istanbul":20230017,
		"Izmir":5250017,
		"Antalya":2250017,
		"Sivas":950017,
	}

	for key, value :=range statePopulations {
		fmt.Println(key,value)
	}
}
